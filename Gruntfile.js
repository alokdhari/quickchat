/// <vs />
/// <binding ProjectOpened = 'watch:scripts' />
/// <vs BeforeBuild = 'copy' />
module.exports = function(grunt)
{ // Project configuration.

    var appConfig = {
        app: 'app',
        dist: 'dist'
      };

    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        cdnify: 'grunt-google-cdn'
    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ["dist/*"],
        bower: {
            install: {
                options: {
                    copy: false
                }
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'scripts/account/**/*.js', 'scripts/app/**/*.js']
        },
        uglify: {
            options: {
                sourceMap: true,
                banner: '/*! dToRrQuE code .. <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */',
                mangle: false,
                quoteStyle: 1
            },
            all: {
                files: {
                    'app/scripts/all.js': ['app/scripts/**/*/js']
                }
            },
            dist: {
                files: {
                    'dist/scripts/ugl-E.js': ['app/scripts/**/*.js']
                }
            }
        },
        watch: {
            scripts: {
                files: ['app/scripts/**/*.js'],
                tasks: ['concat'],
                options: {
                    livereload: true,
                    spawn: false
                }
            },
            html: {
                files: ['app/**/*.html'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['app/styles/**/*.css'],
                options: {
                    livereload: true
                }
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            all: {
                files: [
                    {
                        expand: true,
                        cwd: 'aap/scripts',
                        src: '**/*.js',
                        dest: 'aap/scripts'
                    }
                ]
            }
        },
        // Replace Google CDN references
        cdnify: {
          dist: {
            html: ['dist/*.html']
          }
        },
        connect: {
          options: {
            port: 9000,
            // Change this to '0.0.0.0' to access the server from outside.
            hostname: 'localhost',
            livereload: 35729
          },
          livereload: {
            options: {
              open: true,
              middleware: function (connect) {
                return [
                  connect.static('.tmp'),
                  connect().use(
                    '/bower_components',
                    connect.static('./bower_components')
                  ),
                  connect().use(
                    '/app/styles',
                    connect.static('./app/styles')
                  ),
                  connect.static(appConfig.app)
                ];
              }
            }
          },
          test: {
            options: {
              port: 9001,
              middleware: function (connect) {
                return [
                  connect.static('.tmp'),
                  connect.static('test'),
                  connect().use(
                    '/bower_components',
                    connect.static('./bower_components')
                  ),
                  connect.static('/app')
                ];
              }
            }
          },
          dist: {
            options: {
              open: true,
              base: '<%= yeoman.dist %>'
            }
          }
        },
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */',
                separator: '\n'
            },
            allFiles: {
                files: {
                    'app/scripts/final.js': ['app/scripts/**/*.js']
                }
            }
        },
        // Replace Google CDN references
        cdnify: {
          dist: {
            html: ['dist/*.html']
          }
        },
        cssmin: {
            options: {
                sourceMap: true
            },
            main: {
                files: [{
                  expand: true,
                  cwd: 'app/styles',
                  src: ['*.css', '!*.min.css'],
                  dest: 'app/styles/',
                  ext: '.min.css'
                }]
            }
        },
        copy:{
            main: {
                files: [
                    { 
                        expand: true, 
                        src: ['bower_components/angular/*.min.js', 'bower_components/angular/*.min.js.map'], 
                        dest: 'Scripts/vendor/angular/', 
                        flatten: true, 
                        filter: 'isFile' 
                    }                    
                ]
            },
            dist:{
                files: [
                    { 
                        expand: true, 
                        src: ['app/styles/*.min.css', 'app/styles/*.min.css.map'], 
                        dest: 'dist/styles', 
                        flatten: true, 
                        filter: 'isFile' 
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'app',
                        dest: 'dist',
                        src: [
                            '*.{ico,png,txt}',
                            '*.html',
                            'images/{,*/}*.{webp}',
                            'styles/fonts/{,*/}*.*'
                        ]
                    }  
                ]
            }
        }
    });

    //require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-npm-install');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy','concat:allFiles']);
    grunt.registerTask('serve', ['connect:livereload', 'watch']);
    grunt.registerTask('build', ['clean', 'concat:allFiles', 'cssmin', 'uglify', 'copy:dist', 'cdnify']);
};