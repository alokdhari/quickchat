'use strict';

/**
 * @ngdoc function
 * @name quickChatApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the quickChatApp
 */
angular.module('quickChatApp')
  .controller('MainCtrl', function ($scope, $state, sessions, $firebaseObject) {
    $scope.title = 'Main Page';

    $scope.enterSession = function(){
    	var sessionExists = sessions.hasChild($scope.sessionId);
		if(sessionExists){
		$state.go('session', {'id': $scope.sessionId});
		}    
	};
  });
