'use strict';

/**
 * @ngdoc function
 * @name quickChatApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the quickChatApp
 */
angular.module('quickChatApp')
  .controller('sessionController', function ($scope, $state, $rootScope, localStorageService, sessions, session, $firebaseArray, $firebaseObject) {
    $scope.sessionId = $state.params.id;

    $scope.getRandomString = function()
	{
	    var text = '';
	    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	    for( var i=0; i < 7; i++ ){
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    return text;
	};

	$scope.sessions = sessions;

	if($scope.sessionId === 'new')
	{				
		$scope.newSession = {};
		$scope.newSession.Id = $scope.getRandomString();
	}
	else{		
		$scope.session = session;
		session.$bindTo($scope, 'session');
	}

	$scope.createSession = function(){
		var user = {'Id': 1, 'userName': $scope.user};
		$rootScope.currentUser = user;
		$scope.newSession.users = [user];
		$scope.newSession.chat= [];
		$scope.newSession.events = [];
		$scope.newSession.events.push(getEventFromDescription('Session Created.'));
		$scope.sessions.child($scope.newSession.Id).set($scope.newSession);
		localStorageService.set('currentUser', user);
		$state.go('session', {'id': $scope.newSession.Id});
	};

	$scope.addUserToSession = function()
	{	
		var userId = $scope.session.users.length + 1;
		var user = {'Id': userId, 'userName': $scope.newUser};
		$scope.session.users.push(user);
		$scope.session.events.push(getEventFromDescription($scope.newUser.userName + ' joined the session.'));
		localStorageService.set('currentUser', user);
		$state.reload();
	};

	$scope.sendMessage = function(){
		var chatItem = {};
		chatItem.message = $scope.chatMessage + '';
		chatItem.user = $rootScope.currentUser;
		if(!$scope.session.chat){
			$scope.session.chat = [];
		}

		$scope.session.chat.push(chatItem);
		$scope.chatMessage = '';
	};

	// Just check if the current user is in the session.. otherwise add them.
	if($rootScope.currentUser && $scope.sessionId !== 'new'){
		var userFound = false;
		$scope.session.$loaded(function(){
			angular.forEach($scope.session.users, function(item){
				if(item.userName === $rootScope.currentUser.userName){
					userFound = true;
					return;
				}
			});

			if(!userFound){
				var userId = $scope.session.users ? $scope.session.users.length + 1 : 1;
				var user = {'Id': userId, 'userName': $scope.newUser};
				$scope.session.users.push($rootScope.currentUser);
			}
		});
	}

	$scope.$on('$destroy', function() {
		debugger;
		if($scope.session){
			if($scope.session.users && Object.size($scope.session.users) == 1){
				//Remove the session and all the data.
				$firebaseObject($scope.sessions.child(session.Id)).$remove();
				return;
			}
			else{
				if($scope.session.users){
					var userObject = $firebaseObject($scope.sessions.child(session.Id).child('users'));
			    userObject.$loaded(function(){

						angular.forEach($scope.session.users, function(item, $index){

							if($rootScope.currentUser && item.userName === $rootScope.currentUser.userName){
								delete userObject[$index];
								userObject.$save();
								return;
							}
						});
					}); 
				}  
			}				
		}	
  	});

  	Object.size = function(obj) {
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};

	function getEventFromDescription(description){
		var event = {};
		event.time = new Date();
		event.description = description;
		return event;
	}
});