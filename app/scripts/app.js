'use strict';

/**
 * @ngdoc overview
 * @name quickChatApp
 * @description
 * # quickChatApp
 *
 * Main module of the application.
 */
var app = angular
  .module('quickChatApp', [
    'ui.router',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'LocalStorageModule'
  ])
  .config(function ($urlRouterProvider, $stateProvider, $locationProvider, localStorageServiceProvider) {

    $stateProvider.state('home', {
        templateUrl: 'views/main.html', 
        url: '/home',
        controller: 'MainCtrl',
        data: { pageTitle: 'Home' }
    })
    .state('session', {
        templateUrl: 'views/session.html',
        url: '/session/:id',
        controller: 'sessionController',
        data: { pageTitle: 'Session' },
        resolve: {
            sessions: function(){
                var ref = firebase.database().ref();
                return ref.child('sessions');
            },
            session: function($stateParams, $firebaseObject){
                if($stateParams !== 'new'){
                    var ref = firebase.database().ref();
                    return $firebaseObject(ref.child('sessions').child($stateParams.id));
                }       
            }
        }
    });

    $urlRouterProvider.otherwise('/home');
    $locationProvider.hashPrefix('');

    localStorageServiceProvider.setPrefix('quickChat');

  });

app.run(['$state', '$rootScope', 'localStorageService', function($state, $rootScope, localStorageService) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
        $rootScope.currentUser = localStorageService.get('currentUser');
        $rootScope.pageTitle = toState.data.pageTitle;
    });
}]);
